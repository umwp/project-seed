Project Name
============
Project description here
![Some description #title](imageurl.jpg)

Install
-------

```bash
npm install --save git+https://bitbucket.com/<org>/<project>
```

Features
--------

* Feature 1
* Feature 2
* Feature 3
* Feature 4

Documentation
-------------
[Link text](http://google.com)

Linting
-------
Lint your code! Install [`SublimeLinter-eslint`](https://github.com/roadhump/SublimeLinter-eslint) and get linting right in Sublime Text. We follow the [Airbnb Javascript Styleguide](https://github.com/airbnb/javascript). Submit a pull request if the linter throws an error when it shouldn't.

```bash
make lint
```

Test
----
[Mocha](http://visionmedia.github.io/mocha/) BDD style testing with [should.js](https://github.com/shouldjs/should.js).

```bash
npm test #alias `make test`
```

License
-------
Proprietary
