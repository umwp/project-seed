test:
	@NODE_ENV=test ./node_modules/.bin/mocha --harmony
lint:
	./node_modules/.bin/eslint --ignore-path .gitignore .
.PHONY: test
